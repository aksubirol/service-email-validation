/* globals describe, it */
'use strict'

const expect = require('chai').expect
const assert = require('chai').assert
const db = require('../models')
const validateRegex = require('../validations/validateRegex')
const validateSmtp = require('../validations/validateSmtp')
const validateDb = require('../validations/validateDb')
const createEmail = require('../validations/createEmail')
const removeEmail = require('../validations/removeEmail')

describe('test emails', () => {
  it('regex invalid test', async () => {
    const regexInvalidEmail = 'w[]23%$@test.com'
    const valid = validateRegex(regexInvalidEmail)

    expect(valid).to.deep.equal(false)
  })

  it('db invalid test', async () => {
    const dbInvalidEmail = 'w[]23%$@test.com'
    await createEmail({ email: dbInvalidEmail, validity: false }, db)

    const valid = await validateDb(dbInvalidEmail, db)

    await removeEmail({ email: dbInvalidEmail }, db)
    expect(valid).to.deep.equal('invalid')
  })

  it('smtp invalid test', async () => {
    const smtpInvalidEmail = 'support@gmail.com'

    try {
      await validateSmtp(smtpInvalidEmail)

      assert.fail('expected exception not thrown')
    } catch (error) {
      expect(error.message).to.be.oneOf([
        'false',
        'Could not determine validity'
      ])
    }
  })

  it('regex valid test', async () => {
    const regexValidEmail = 'test@test.com'
    const valid = validateRegex(regexValidEmail)

    expect(valid).to.deep.equal(true)
  })

  it('db valid test', async () => {
    const dbValidEmail = 'demo@gmail.com'
    await createEmail({ email: dbValidEmail, validity: true }, db)

    const valid = await validateDb(dbValidEmail, db)

    await removeEmail({ email: dbValidEmail }, db)
    expect(valid).to.deep.equal('valid')
  })

  it('smtp valid test', async () => {
    const smtpValidEmail = 'aksubirol@gmail.com'

    try {
      const valid = await validateSmtp(smtpValidEmail)

      expect(valid).to.deep.equal(true)
    } catch (error) {
      console.error(error)
    }
  })
})
