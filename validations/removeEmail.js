module.exports = ({ email }, db) => {
  return new Promise((resolve, reject) => {
    const { Email } = db

    Email.findOne({ where: { email } }).then((email) => {
      return email
        .destroy()
        .then(() => resolve({ deleted: true }))
        .catch(reject)
    })
  })
}
