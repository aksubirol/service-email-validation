const createEmail = ({ email, validity }, db) => {
  return new Promise((resolve, reject) => {
    const { Email } = db

    Email.create({ email, validity }).then((email) => {
      return resolve(email.id)
    })
  })
}

module.exports = createEmail
