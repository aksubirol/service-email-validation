module.exports = (email, db) => {
  return new Promise((resolve, reject) => {
    const { Email } = db

    try {
      Email.findOne({ where: { email } }).then((email) => {
        if (email === null) {
          return resolve('not found')
        } else {
          if (email.validity) {
            return resolve('valid')
          } else {
            return resolve('invalid')
          }
        }
      })
    } catch (error) {
      return reject(error)
    }
  })
}
