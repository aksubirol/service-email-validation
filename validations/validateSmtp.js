'use-strict'

const dns = require('dns')
const net = require('net')
const errorCodes = [
  '500',
  '501',
  '502',
  '503',
  '504',
  '521',
  '541',
  '550',
  '551',
  '552',
  '553',
  '554',
  '555',
  '556'
]

module.exports = (email) => {
  return new Promise((resolve, reject) => {
    const host = email.split('@')[1]

    dns.resolveMx(host, (error, response) => {
      if (error) {
        return reject(new Error(`Error in resolving mx for ${host}`))
      }

      const server = response[0].exchange
      const client = new net.Socket()
      let hasError = false

      client.connect(25, server)

      console.log(`Connected to: ${server}`)

      client.write(`EHLO ${server}\r\n`)
      client.write('MAIL FROM: <test@mail.com>\r\n')
      client.write(`RCPT TO: <${email}>\r\n`)
      client.write('QUIT\r\n')

      client.on('data', (data) => {
        // data is a buffer object, needs to be casted to string
        const response = data.toString()
        console.log(response)

        errorCodes.forEach((code) => {
          if (response.indexOf(code) !== -1) {
            hasError = true

            if (code === 550) {
              console.error(`Email address: ${email} is not valid.`)
              client.destroy()

              return reject(new Error(false))
            }

            return reject(new Error('Could not determine validity'))
          }
        })
      })

      client.on('error', (error) => {
        return reject(new Error(`Socket error. ${error}`))
      })

      client.on('close', () => {
        console.log('Connection closed.')

        if (!hasError) {
          console.log(`Email address: ${email} is valid.`)

          return resolve(true)
        }
      })
    })
  })
}
