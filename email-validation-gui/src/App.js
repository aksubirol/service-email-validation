import React, { useState } from 'react'
import fetch from 'isomorphic-fetch'
import './App.css'

const App = () => {
  const [email, setEmail] = useState('')
  const [valid, setValid] = useState(null)

  const change = (e) => {
    setEmail(e.target.value)
  }

  const validate = async () => {
    if (email !== '') {
      const response = await fetch(
        `http://ec2-34-254-242-127.eu-west-1.compute.amazonaws.com:3001?email=${email}`
      )
      const myJson = await response.json()
      let validity = ''
  
      if (!myJson) {
        validity = `${email} is not a valid email address`
      } else {
        validity = `${email} is valid`
      }
  
      setValid(validity)
    } else {
      setValid('Please enter en email address to continue')
    }
  }

  return (
    <div className='App'>
      <h1>Email Validator</h1>
      <input
        type='text'
        className='email'
        placeholder='Enter email to validate'
        value={email}
        onChange={change}
      />
      <button className='button' onClick={validate}>
        Validate
      </button>
      <div className='info'>{valid}</div>
    </div>
  )
}

export default App
