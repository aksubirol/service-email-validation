'use strict'

const DataType = require('sequelize')

module.exports = {
  fields: {
    id: {
      type: DataType.UUID,
      defaultValue: DataType.UUIDV4,
      primaryKey: true
    },
    email: { type: DataType.STRING },
    validity: { type: DataType.BOOLEAN }
  },
  options: {
    engine: 'InnoDB',
    freezeTableName: true,
    tableName: 'email',
    timestamps: true,
    paranoid: true
  }
}
