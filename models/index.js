'use strict'

const Sequelize = require('sequelize')
const env = process.env.NODE_ENV || 'development'
const config = require(`${__dirname}/../config/database.json`)[env]
const models = {}

const sequelize = config.use_env_variable
  ? new Sequelize(process.env[config.use_env_variable])
  : new Sequelize(config.database, config.username, config.password, config)

const modelList = {
  Email: require('./Email')
}

Object.keys(modelList).forEach((model) => {
  models[model] = sequelize.define(
    model.toLowerCase(),
    modelList[model].fields,
    modelList[model].options
  )
})

sequelize.sync()

models.sequelize = sequelize
models.Sequelize = Sequelize

module.exports = models
