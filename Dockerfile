FROM node:10.1.0-slim as base

RUN mkdir -p /srv/service

WORKDIR /srv/service

FROM base AS dependencies

COPY package*.json ./
COPY ./config/database.example.json /srv/service/config/database.json
COPY ./config/server.example.json /srv/service/config/server.json

RUN npm i --production

FROM base AS build

COPY . /srv/service

COPY --from=dependencies /srv/service/node_modules /srv/service/node_modules

FROM build as release

EXPOSE 3000

CMD ["npm", "start"]