# Atheneum Email Validation API & GUI

Email Validation API

This project provides an API for validating an email address.
It has a gui that enables communication with email validation API.
Input is en email address and the output is valid or invalid based on the email address input.

The project is deployed on AWS EC2 and can be access through the following address:
`http://ec2-34-254-242-127.eu-west-1.compute.amazonaws.com:3000`

The project's service is accessible through the following address:
`http://ec2-34-254-242-127.eu-west-1.compute.amazonaws.com:3001`

Steps of validation:

1. Database validation - Checks if input exists in the database
   -> if it exists, it returns the validity column value in the database and stops execution of the other steps.
   -> if it does not exist, continues validation.
2. Regex validation - Tests input with standard format
   -> if fails, return invalid message, creates a new row in the database as invalid
   -> if succeeds, continue execution of validation
3. SMTP validation - Tests input through SMTP

- Gets MX server values from the entered email
- Connects through net socket from port 25 to the mx server
- Sends initial commands to SMTP server until `RCPT TO` to check validation of the input
  -> if it receives error code 550, return invalid message, creates a new row in the database as invalid
  -> if it does not receive error, return valid message, creates a new row in the database as valid

Logic:

Database validation implemented first to check the database before making SMTP calls.
Regex validation implemented according to RFC2822 rules as much as possible. (99.99% match)
SMTP validation error codes used to match the invalid output from server.
Used GET method for ease of access.

Further improvements:

- Fixing the pipelines by implementing an external sql instance.
- Separate GUI folder from the main project folder to stand-alone app.
- Implementation of Environment variables as connection configs.
- Improvements can be made in the Docker setup to increase deployment performance.
- Implement CodeCommit, CodeBuild and CodeDeploy enable CodePipeline in AWS to enable CD.

Timing:

Total time spent on the project is 5 hours.

Issues Faced:

- Docker connections.
- Pipelines configuration to external sources.

## Environment Setup via Homebrew

- $ ruby -e "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/master/install)"
- \$ brew install nvm
- \$ nvm install 8.1.2

## App Setup

- `$ cp config/server.example.json config/server.json`
- `$ cp config/database.example.json config/database.json`
- `$ npm i`

## App Start

- `$ npm start`

## Unit Tests

- `$ npm run test`

### Sample call to email validation

`http://{service_address}?email={email_to_be_validated}`

### Deployment

Local Environment

run `$ docker-compose up`

Application can be accessed from `localhost:3000`

### Libraries Used:

- Sequelize
- Mysql
- Nodemon for Development Watch Mode
- Mocha for Unit Tests
