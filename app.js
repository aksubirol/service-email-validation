'use strict'

const config = require('./config')
const http = require('http')
const port = process.env.PORT || config.server.port
const path = require('path')
const pkg = require(path.resolve(process.env.PWD, 'package.json'))
const serviceName = pkg.name.split('service-').pop()
const validations = require('./validations')
const db = require('./models')

const resolve = async (email, request, response) => {
  response.setHeader('Access-Control-Allow-Origin', '*')
  response.setHeader('Content-Type', 'application/json')

  try {
    const valid = await validations(db, email)

    response.statusCode = 200
    response.end(JSON.stringify(valid), 'utf8')
  } catch (error) {
    console.error(error)

    response.statusCode = 400
    response.end(error.toString())
  }
}

http
  .createServer((request, response) => {
    try {
      if (request.method === 'OPTIONS') {
        response.setHeader('Access-Control-Allow-Origin', '*')
        response.setHeader('Access-Control-Allow-Headers', 'Content-Type')
        response.end()
      } else {
        const baseURL = 'http://' + request.headers.host + '/'
        const fullURL = new URL(request.url, baseURL)
        const email = fullURL.searchParams.get('email')

        if (email) {
          resolve(email, request, response)
        } else {
          console.error('Parameter email does not exist')
          response.statusCode = 500
          response.end()
        }
      }
    } catch (error) {
      console.error({ error }, 'error in processing request')
      response.statusCode = 500
      response.end()
    }
  })
  .on('clientError', (_, socket) => socket.end('HTTP/1.1 400 Bad Request'))
  .listen(port, () => {
    console.log(`${serviceName} started running on port: ${port}`) // eslint-disable-line
  })
